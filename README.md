# Htia BP Last Activity

Creates last_activity usermeta & bp_activity records for new users. This way, users show up immediately after registration and/or profile update in the Buddypress member list when sorting on activity or registration date.


### Installing

Just clone or download this repository in your wp-content/plugins/ folder.


## Authors

* **Daniel van der Mierden**


## License

This project is licensed under the MIT License