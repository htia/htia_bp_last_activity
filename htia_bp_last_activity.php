<?php
/**
 * Plugin Name: Htia Buddypress Last Activity registration
 * Plugin URI:  https://www.high-teck.com
 * Description: Record user last activity for BP. This allows users that are created in the backoffice to be visible immediately after saving. Otherwise the user would show up only after first login.
 * Author:      Daniel van der Mierden <dvdmierden@high-teck.com>
 * Author URI:  https://www.high-teck.com
 * Version:     1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/* hook on registration */
add_action( 'user_register', 'htia_registration_create_last_activity', 10, 1);

/* hook on user update */
add_action('edit_user_profile_update', 'htia_registration_create_last_activity');

/**
 *	Create last activity record in Buddypress table - if available.
 *	Since there's no unique key on user_id, remove it first.
 *
 *	@param int $user_id 
 *
 *	@see wp-content/plugins/buddypress/bp-members/bp-members-functions.php @line 1288
 *	@return void
 */
function htia_registration_create_last_activity( $user_id ) {
	global $wpdb;
	
	if ($bp = buddypress()) {	
		update_user_meta( $user_id, 'last_activity', date('Y-m-d H:i:s', time()) );
		$sql = "DELETE FROM {$bp->members->table_name_last_activity} WHERE user_id = {$user_id} ";
		$wpdb->query($sql);
		
		
		$sql = "INSERT INTO {$bp->members->table_name_last_activity}  (user_id, component, type, action, content, primary_link, item_id, date_recorded ) 
			(
				SELECT user_id, '{$bp->members->id}' as component, 'last_activity' as type, '' as action, '' as content, '' as primary_link, 0 as item_id, meta_value AS date_recorded
				FROM {$wpdb->usermeta}
				WHERE
					meta_key = 'last_activity' 
					AND user_id = {$user_id}
			);";
	
	
		$wpdb->query( $sql );
	}
}